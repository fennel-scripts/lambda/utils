#!/usr/bin/env fennel
;;carbon copy clone of the botcmd library, and then extended, might have incompaitibilities
(local utils {})
(local tcat table.concat)
(local stow-actions {:stow "--stow" :unstow "--delete" :restow "--restow"})

(lambda utils.any->str [x ?sep]
  (let [sep (or ?sep " ")]
	(match (type x)
	  :table	(tcat x sep)
	  :string	x
	  _ (tostring x))))

(lambda utils.exec [command ?sep]
  "execute:
execute system command defined in the first argument(string or squential table) and
an optional separator as the second argument
the default separator is ' ' (a single space) so you dont have to pad the strings."
  (let [sep (or ?sep " ")]
	(match (type command)
	  :table	(os.execute (tcat command sep))
	  :string	(os.execute command))))

(lambda utils.tprint [command ?sep]
  "print a table or string, with an optional separator argument that is passed to table.concat"
  (let [sep (or ?sep " ")]
	(match (type command)
	  :table	(print (tcat command sep))
	  :string	(print command))))
(lambda utils.exec* [command ?sep]
  "execute (dummy):
execute system command defined in the first argument(string or squential table) and
an optional separator as the second argument
the default separator is ' ' (a single space) so you dont have to pad the strings."
  (utils.tprint command ?sep)
  )



(lambda utils.install [{: src : tgt :srcsep ?srcsep :tgtsep ?tgtsep :sudo ?sudo}]
  "wrapper over the install command"
	(print (utils.any->str src ?srcsep) "==>" (utils.any->str tgt ?tgtsep))
	(utils.exec [(if ?sudo "sudo" "") "install" "--compare" (utils.any->str src ?srcsep) (utils.any->str tgt ?tgtsep)] " ")
	)
(lambda utils.stow [{: dir :tgt ?tgt :target ?target :action ?action : pkgs}]
  (lambda stow [{: dir : tgt :action ?action : pkgs}]
	(var packages pkgs)
	(when (= (type packages) :string)		;make sure the packages(pkgs) is a list/table
	  (set packages [packages]))		;rather than a string, later undone by table.concat(tcat)
	(utils.exec ["stow" "-v" "-d" dir "-t" tgt (. stow-actions (or ?action "stow")) (tcat packages " ") ]))
  (stow {: dir :tgt (or ?tgt ?target) :action ?action : pkgs}))

(lambda utils.mkdir [{: dirs :sep ?sep :sudo ?sudo}]
  "make directory :args
{:dirs directory or a list of directorys to create
 :sudo should I use sudo(optional)}"
  ;;(print "****use mkdir* for backwards compatability****")
  ;;(table.insert args 1 "mkdir -pv ")
  ;;(utils.exec [(if ?sudo "sudo" "") "mkdir" "--parents" "--verbose" (if (= (type dirs) "table") (tcat dirs ?sep) dirs)])
  (utils.exec [(if ?sudo "sudo" "") "mkdir" "--parents" "--verbose" (utils.any->str dirs ?sep)])
  )

(lambda utils.mkdir* [{: dirs :sep ?sep :sudo ?sudo}]
  "make directory(dummy) :args
{:dirs directory or a list of directorys to create
 :sudo should I use sudo(optional)}"
  ;;(print "****use mkdir* for backwards compatability****")
  ;;(table.insert args 1 "mkdir -pv ")
  ;;(utils.exec [(if ?sudo "sudo" "") "mkdir" "--parents" "--verbose" (if (= (type dirs) "table") (tcat dirs ?sep) dirs)])
  (utils.exec* [(if ?sudo "sudo" "") "mkdir" "--parents" "--verbose" (utils.any->str dirs ?sep)])
  )
(fn utils.rsync [{: src : srcs : tgt}]
  "wrapper around rsync
args: src or srcs, file(s) to sync
tgt: target directory, where to sync to"
  (lambda rsync [{: src : tgt}]
	"inner lambda to ease argument naming (srcs and src is the same after some masaging)"
	(utils.exec ["rsync" "--progress"  "--preallocate" src tgt] " "))
  (rsync {:src (utils.any->str (or src srcs) " ") : tgt}))

utils
